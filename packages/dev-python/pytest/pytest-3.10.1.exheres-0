# Copyright 2013 Jakob Nixdorf <flocke@shadowice.org>
# Distributed under the terms of the GNU General Public License v2

require pypi setup-py [ import=setuptools has_bin=true python_opts='[sqlite]' ]

SUMMARY="Powerful and scalable testing library for python development"
DESCRIPTION="
The py.test library provides full-featured and mature testing environment
that can scale from simple unit testing to complex functional testing.
It includes many common testing methods as well as an extensive plugin and
customization system.
"

BUGS_TO="flocke@shadowice.org"

UPSTREAM_CHANGELOG="https://pytest.org/latest/changelog.html"
UPSTREAM_DOCUMENTATION="https://pytest.org/latest/contents.html"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-python/atomicwrites[>=1.0][python_abis:*(-)?]
        dev-python/attrs[>=17.4.0][python_abis:*(-)?]
        dev-python/more-itertools[>=4.0.0][python_abis:*(-)?]
        dev-python/pluggy[>=0.7][python_abis:*(-)?]
        dev-python/py[>=1.5.0][python_abis:*(-)?]
        dev-python/setuptools_scm[python_abis:*(-)?]
        dev-python/six[>=1.10.0][python_abis:*(-)?]
        python_abis:2.7? (
            dev-python/funcsigs[python_abis:2.7]
            dev-python/pathlib2[>=2.2.0][python_abis:2.7]
        )
        python_abis:3.4? ( dev-python/pathlib2[>=2.2.0][python_abis:3.4] )
        python_abis:3.5? ( dev-python/pathlib2[>=2.2.0][python_abis:3.5] )
    test:
        dev-python/hypothesis[>=3.5.2][python_abis:*(-)?]
        python_abis:2.7? ( dev-python/mock[python_abis:*(-)?] )
    suggestion:
        dev-python/pytest-twisted[python_abis:*(-)?] [[
            description = [ write tests for twisted apps ]
        ]]
        dev-python/mock[python_abis:*(-)?] [[
            description = [ mocking functionality ]
        ]]
        dev-python/nose[python_abis:*(-)?] [[
            description = [ run test suites written for nose ]
        ]]
        dev-python/pexpect[python_abis:*(-)?] [[
            description = [ support for testing py.test and py.test plugins ]
        ]]
        dev-python/pytest-xdist[python_abis:*(-)?] [[
            description = [ to distribute tests to CPUs and remote hosts ]
        ]]
"

test_one_multibuild() {
    # Last checked: pytest-3.10.1 on python 2.7.15 and 3.7.2.
    local disabled_tests=(
        'not TestTrialUnittest'  # Fails when Twisted is installed.
        'and not pyc_vs_pyo'  # Tries to write bytecode in /.
        'and not test_installed_plugin_rewrite'  # Loading plugins disabled.
        'and not test_pathlib'  # Hangs or works too long.
        'and not test_pdb'  # Hangs or works too long.
        'and not test_plugin_preparse_prevents_setuptools_loading'  # Plugins obviously.
        'and not test_preparse_ordering_with_setuptools'  # Loading plugins disabled.
        'and not test_runtest_location_shown_before_test_starts'  # Hangs or works too long.
        'and not test_setuptools_importerror_issue1479'  # Probably disabled plugins again.
        'and not xdist'  # xdist plugin disabled.
    )
    # Only load chosen plugins for reproducible testing.
    PYTHONPATH=$(echo $(pwd)/build/lib*) \
        PYTEST_DISABLE_PLUGIN_AUTOLOAD=1 PYTEST_PLUGINS="hypothesis" \
        edo ${PYTHON} -B src/pytest.py testing \
        -k "${disabled_tests[*]}" --maxfail=5
}

install_one_multibuild() {
    setup-py_install_one_multibuild

    edo cp -a "${IMAGE}"/usr/$(exhost --target)/bin/py.test{,-$(python_get_abi)}
}

