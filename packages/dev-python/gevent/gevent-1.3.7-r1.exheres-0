# Copyright 2017 Arnaud Lefebvre <a.lefebvre@outlook.fr>
# Distributed under the terms of the GNU General Public License v2

require pypi setup-py [ import=setuptools ]

SUMMARY="Coroutine-based Python networking library"
HOMEPAGE="http://www.gevent.org"

SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

LICENCES="MIT"

# Most tests need internet access
RESTRICT="test"

DEPENDENCIES="
    build+run:
        dev-libs/libev
        dev-python/dnspython[>=0.4.10][python_abis:*(-)?]
        dev-python/greenlet[>=0.4.14][python_abis:*(-)?]
        dev-python/idna[>=0.4.10][python_abis:*(-)?]
        dev-python/zopeevent[python_abis:*(-)?]
        dev-python/zopeinterface[python_abis:*(-)?]
        net-dns/c-ares[>=1.14]
    test:
        dev-python/coverage[>=4.0][python_abis:*(-)?]
        dev-python/futures[python_abis:*(-)?]
        dev-python/mock[python_abis:*(-)?]
        dev-python/objgraph[python_abis:*(-)?]
        dev-python/psutil[python_abis:*(-)?]
        dev-python/requests[python_abis:*(-)?]
"

pkg_setup() {
    export LIBEV_EMBED=false
    export CARES_EMBED=false
    export EMBED=false
}

prepare_one_multibuild() {
    # Those files are specific python version files
    [[ $(python_get_abi) == 2.* ]] && edo rm -rf \
        src/gevent/_socket3.py \
        src/gevent/_ssl3.py

    [[ $(python_get_abi) == 3.* ]] && edo rm -rf \
        src/gevent/_socket2.py \
        src/gevent/_ssl2.py \
        src/gevent/_util_py2.py
}

